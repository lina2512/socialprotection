if (window.matchMedia("(min-width: 1366px)").matches) {
    $('.inner-container__element').hover(function () {
        $(this).removeClass('dishover');
        $(this).addClass('hover');
    }, function () {
        $(this).removeClass('hover');
        $(this).addClass('dishover');
    });

    $('.doc-element').hover(function () {
        $(this).removeClass('dishover');
        $(this).addClass('hover');
    }, function () {
        $(this).removeClass('hover');
        $(this).addClass('dishover');
    });
}

var item = true;
$('.link-menu').hover(function(){
    if( item === true) {
        item = false;

        $('.menu-hover').fadeIn(function () {
            item = true;
        });
    }
}, function () {
    $('.menu-hover').fadeOut();
});

$('header .search').on('click', function(){
    var ths = $('.search-header');
    if(ths.hasClass('active')) {
        ths.removeClass('active');
        ths.addClass('disactive');
    } else {
        ths.removeClass('disactive');
        ths.addClass('active');
    }
});

$(document).ready(function(){
    $('.captcha .checkbox').on('click touchStart', function(){
        var parentCont = $(this).closest('form');
        var parentCaptcha = $(this).closest('.captcha');
        var newElement = $("<button class=\"btn\">Отправить</button>");
        parentCont.append(newElement);
        parentCaptcha.remove();
    });
});

$(function() {
    $(window).scroll(function() {
        if($(this).scrollTop() != 0) {
            $('.to-top').fadeIn();
        } else {
            $('.to-top').fadeOut();
        }
    });
    $('.to-top').on('click', function() {
        $('body,html').animate({scrollTop:0},800);
    });
});

//call

function blur(){
    $('.wrapper').addClass('blur');
}

function unBlur(){
    $('.wrapper').removeClass('blur');
}

$(".modal__call").fancybox({
    beforeShow: blur,
    beforeClose : unBlur,
    touch: false
});

$(".vacancies_btn").fancybox({
    beforeShow: blur,
    beforeClose : unBlur,
    touch: false
});

$(".burger").fancybox({
    beforeShow: blur,
    beforeClose : unBlur,
    touch: false
});

$('.modal-left').on('click', function(){
    console.log('zakr');
    parent.$.fancybox.close();
});

if (window.matchMedia("(min-width: 1366px)").matches) {
    $('.footer_ipad').remove();
}

if (window.matchMedia("(max-width: 1365px)").matches) {
    $('.footer_full').remove();
}

if (window.matchMedia("(max-width: 1365px)").matches) {
    $('.link-menu').each(function(){
        var ths = $(this).find('.link-copy');
        var href = ths.attr('href');
        var text = ths.text();
        var menu = $(this).find('ul');
        menu.prepend($("<li><a href=\"" + href +  "\">" + text + "</a></li>"));
    });

    $('.link-copy').on('click', function(){
        event.preventDefault();

        $.fancybox.open({
            src  : '.menu-hover',
            type : 'inline',
            touch: false
        });
    });
}

$(document).ready( function() {
    $(".modal-form__file input[type=file]").change(function(){
        var filename = $(this).val().replace(/.*\\/, "");
        $("#filename").text(filename);
        $(".upload-photo").css("display", "flex");
        $('.upload-photo__none').css("display", "none");
    });
});
